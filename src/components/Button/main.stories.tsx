import { Meta } from '@storybook/react/types-6-0'
import Button from '.'
import { IButtonProps } from './interfaces'

export default {
  title: 'Button',
  component: Button,
  argTypes: {
    color: { control: 'color' }
  }
} as Meta

export const Basic = (args: IButtonProps) => <Button {...args} />

Basic.args = {
  text: 'Ola',
  color: 'red'
}
