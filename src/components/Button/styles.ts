import styled from 'styled-components'
import { IButtonProps } from './interfaces'

export const Button = styled.button`
  background-color: ${(props: IButtonProps) =>
    props.color ? props.color : 'red'};
  color: #fff;
  width: 100%;
  height: 100%;
  padding: 3rem;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
