import { IButtonProps } from './interfaces'
import * as S from './styles'

const Button = (props: IButtonProps) => {
  const { text, color } = props

  return <S.Button color={color}>{text}</S.Button>
}
export default Button
