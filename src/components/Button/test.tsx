import { render } from '@testing-library/react'
import Button from '.'

describe('<Button />', () => {
  it('should render the text correctly', () => {
    const text = 'Daniel'
    const { container } = render(<Button text={text} />)

    expect(container.textContent).toContain(text)
  })

  it('should render the bgColor correctly', () => {
    const bgColor = 'green'
    const { container } = render(<Button color={bgColor} />)

    expect(container.firstChild).toHaveStyle({
      'background-color': bgColor
    })
  })
})
